# LDAP Environment on Docker Compose

Create an LDAP sandbox/playground with Docker Compose to learn, test your
config or ideas, play with, etc.

This repo would not be what it is if not for
[Osixia](https://github.com/osixia/docker-openldap)'s OpenLDAP Docker image.

# Requirements

- Docker
- Python 3.6+
- [pipenv](https://docs.pipenv.org/)
- make

# Initial Setup

        $ pipenv install

Installs required Python packages using ``pipenv``.

The drawback is that you either activate the Python virtualenv once with
``pipenv shell`` (*preferred*) or prepend ``pipenv run`` to every command. If
you go the ``pipenv shell`` route, you only have to activate it *once* and then
run ``invoke`` or ``docker-compose`` as needed. When you're done, you can
``exit`` the shell.

        $ invoke

[Invoke](http://docs.pyinvoke.org/en/latest/) is used to run the *create* and
*destroy* tasks described below. It's a task execution mechanism similar to
``make``. It runs the tasks in *tasks.py*.

# Create

        $ pipenv shell  # activate Python virtualenv
        $ invoke create

1. Creates directories to be mounted in the server container.

    1. ./volumes/var_lib_ldap
    2. ./volumes/etc_ldap_slapd.d
2. Creates the server container.

# Destroy

        $ pipenv shell  # activate Python virtualenv
        $ invoke destroy

1. Destroys all containers.

# Container Lifecycle

        $ pipenv shell  # activate Python virtualenv
        $ docker-compose

``docker-compose`` is used to manage the lifecycle of containers.

Run ``bash`` in OpenLDAP server container.

        $ pipenv shell  # activate Python virtualenv
        $ docker-compose exec openldap bash

# Customize

These files are prime candidates to customize for your needs.

## docker-compose.yml

Uses the distribution of OpenLDAP in a Docker image by
[Osixia](http://www.osixia.net/about/).
