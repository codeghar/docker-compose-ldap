import os

import invoke


@invoke.task
def create(ctx):
    """
    Create the LDAP environment

    Creates OpenLDAP server container.

    :param ctx: Context object passed by invoke to a task
    :return: None
    """
    server_init(ctx)


@invoke.task
def destroy(ctx):
    """
    Destroy the LDAP environment

    Destroys all containers.

    :param ctx: Context object passed by invoke to a task
    :return: None
    """
    ctx.run("docker-compose down", hide=True)


def server_init(ctx):
    """
    Initialize the OpenLDAP server

    :param ctx: Context object passed to the parent task by invoke
    :return: None
    """
    volumes_dir = os.path.join(".", "volumes")
    os.makedirs(volumes_dir, exist_ok=True)
    os.makedirs(os.path.join(volumes_dir, "var_lib_ldap"), exist_ok=True)
    os.makedirs(os.path.join(volumes_dir, "etc_ldap_slapd.d"), exist_ok=True)

    server_container_up(ctx)


def server_container_up(ctx):
    """
    Bring up the OpenLDAP server container

    Since docker-compose is used here, the lifecycle of image and container is offloaded to it.

    :param ctx: Context object passed to the parent task by invoke
    :return: None
    """
    ctx.run("docker-compose up -d openldap", hide=True)
